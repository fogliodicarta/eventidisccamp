const express = require('express');
const cors = require('cors');
const app = express();
app.use(express.json());
app.use(express.static(__dirname + ''));
app.use(cors());
let events = 0;
var port = process.env.PORT || 8080;

//file csv
const csv = require('csv-parser');
const fs = require('fs');
let listEvent =[];
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const csvWriter = createCsvWriter({
  path: 'events.csv',
  header: [
    {id: 'id', title: 'id'},
    {id: 'titolo', title: 'titolo'},
    {id: 'descrizione', title: 'descrizione'},
    {id: 'luogo', title: 'luogo'},
    {id: 'data', title: 'data'},
    {id: 'foto', title: 'foto'}
  ]
});


Date.dateDif = function( date1, date2 ){
    var one_day=1000*60*60*24;
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();
    var difference_ms = date2_ms - date1_ms;
    return Math.round(difference_ms/(one_day)); 
}

function importEventsAndStart(){
    fs.createReadStream('events.csv')
      .pipe(csv())
      .on('data', (row) => {
        console.log(row);
        listEvent[events]=row;
        //console.log(Date.dateDif((new Date(listEvent[events].data)), (new Date())));
        if(Date.dateDif((new Date(listEvent[events].data)), (new Date()))<0){
            listEvent[events].id=events;
            events++;
        }else{
            listEvent.pop();
        }

      })
      .on('end', () => {
        console.log('CSV file successfully processed');
        exportEvents();
        start();
      });
};

function start(){
    app.get('/', function(req, res){
      console.log('\nGET #####################################\n');
      res.json(listEvent);
      //exportEvents();
    });
    app.get('/:indice',function(req,res){
      res.json(listEvent[req.params.indice]);
    });
    app.post('/nuovoevento', function(req, res){
        console.log('\nPOST #####################################\n');
        let id = events;
        let titolo = req.body.titolo;
        let descrizione = req.body.descrizione;
        let luogo = req.body.luogo;
        let data = req.body.data;
        let foto = req.body.foto;

        let added=0;
        
        if(Date.dateDif(dateTod, dateEv)<0){
            console.log(id+'\n'+titolo+'\n'+descrizione+'\n'+data+'\n'+foto);
            listEvent[events]={id:id, titolo:titolo, descrizione:descrizione, data:data, foto:foto, luogo:luogo};
            events++;
            console.log("numero evento: "+events);
            added = 1;
        }
        
        if(added==1){
            res.json(listEvent);
            exportEvents();
        }else{
            res.join('error');
        }
    });
    console.log(JSON.stringify(listEvent));
    
    app.listen(port, () => console.log('Server ready'));
};

function exportEvents(){
  csvWriter
  .writeRecords(listEvent)
  .then(()=> console.log('The CSV file was written successfully'));
};

importEventsAndStart();
